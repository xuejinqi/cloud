const body = require("koa-body");

module.exports = options => {
  return async (ctx, next) => {
    await body(options)(ctx, async () => {
      // 快捷
      if (!ctx.request.body.files) {
        ctx.post = ctx.request.body;
      } else {
        ctx.post = ctx.request.body.fields;
        ctx.file = ctx.request.body.files;
      }
    });

    await next();
  };
};
