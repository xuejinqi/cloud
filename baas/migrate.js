const { bookshelf, Models } = require("./model");

(async () => {
  // 数据class迁移到classTable
  const classInfo = await Models.class.fetchAll();
  for (const key in classInfo) {
    if (!classInfo[key].tables) {
      continue;
    }

    const tables = classInfo[key].tables.split(",");
    for (const _key in tables) {
      await Models.classTable
        .forge({
          baas_id: classInfo[key].baas_id,
          class_id: classInfo[key].id,
          table: tables[_key],
          fetch: 1,
          add: 1,
          update: 1,
          delete: 1,
          auth_id: classInfo[key].auth_id
        })
        .save();
    }
  }
  process.exit();
})();
